from lib2to3.pgen2 import driver
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
import time

browser = webdriver.Chrome(ChromeDriverManager().install())

try:
    browser.get('http://suninjuly.github.io/simple_form_find_task.html')
    
    first_name_input = browser.find_element(By.NAME, 'first_name')
    last_name_input = browser.find_element(By.NAME, 'last_name')
    city_input = browser.find_element(By.NAME, 'firstname')
    country_input = browser.find_element(By.ID, 'country')
    submit_button = browser.find_element(By.ID, 'submit_button')
      
    first_name_input.send_keys("Vasya")
    last_name_input.send_keys('Pupkin')
    city_input.send_keys('Smolensk')
    country_input.send_keys('Erithrea')
    submit_button.click()
    

finally:
    time.sleep(5)
    browser.quit()
    